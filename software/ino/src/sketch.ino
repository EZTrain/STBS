//WARNING: If there is any problem with the rails going in the other way
//Change HIGH to LOW in relay1 relay2 relay3 and revert.
// Info: This code include debuging in the serial

//Adding the stop variable
boolean stop = false;
boolean orange = false;

//Adding the lights
const char red = 11;
const char yellow = 10;
const char green = 9;

//Adding the relays
const char relay1 = 6;
const char relay2 = 5;
const char relay3 = 4;

//Adding the sensors
const char breakSensor = 3;
const char outSensor = 2;

//Adding the COM
const char COMin = 8;
const char COMout = 7;

// Adding the YEL ports
const char YELin = A0;
const char YELout = A1;

//Define detect
int detect = true;

void setup() {
  //Define the outputs
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);

  //Define the inputs
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(8, INPUT);

  // Define the analog pins
  pinMode(A0, INPUT);
  pinMode(A1, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {
  //When receive a signal:
  detect = digitalRead(COMin);
  if(detect == HIGH) {
    //Set stop to false
    stop = false;
    Serial.println("The zone will start");

    //Set the lights to green
    digitalWrite(green, HIGH);
    digitalWrite(red, LOW);
    digitalWrite(yellow, LOW);

    //Powering the rails
    up();

    // Waiting for a few time
    delay(500);
  }

  //When the train goes out the block:
  detect = digitalRead(outSensor);
  if(detect == LOW && orange == false) {
    Serial.println("The zone will stop");
    //Send start to the precedent
    digitalWrite(COMout, HIGH);

    //Set lights to red
    digitalWrite(green, LOW);
    digitalWrite(red, HIGH);
    digitalWrite(yellow, LOW);

    //Set stop to true
    stop = true;

    //Stop to power the COMout
    delay(250);
    digitalWrite(COMout, LOW);
  }

  //If the train enter the breaking zone and needs to stop
  detect = digitalRead(breakSensor);
  if(detect == LOW && stop == true && orange == false) {
    Serial.println("The zone is stopped");
    //Power off the tracks.
    down();
  }

  // To trigger the following script, just replace the YELin of the controller with a button
  
  // If YELin = LOW then set YELout as LOW and disable the orange led and set everything to normal
  detect = digitalRead(YELin);
  if (detect == LOW) {
  	Serial.println(detect);
  	// Stop the infinite signal
	digitalWrite(YELout, LOW);

	// Stop the yellow light and set everything to green
	digitalWrite(yellow, LOW);
	digitalWrite(green, HIGH);
	digitalWrite(red, LOW);
  	
  	orange = false;
  }

  // If YELin = HIGH then set YELout as HIGH and activate the orange led and set orange to true
  detect = digitalRead(YELin);
  if (detect == HIGH) {
  	Serial.println(detect);
	// Set orange to true
	orange = true;
  
	// Send infinite signal
	digitalWrite(YELout, HIGH);

	// Disable all the relays
	up();

	// Disable the other leds
	digitalWrite(green, LOW);
	digitalWrite(red, LOW);

	// Make it blink
	digitalWrite(yellow, HIGH);
	delay(1000);
	digitalWrite(yellow, LOW);
	delay(1000);
  }
}

void down() {
	digitalWrite(relay1, HIGH);
	digitalWrite(relay2, HIGH);
	digitalWrite(relay3, HIGH);
}

void up() {
	digitalWrite(relay1, LOW);
	digitalWrite(relay2, LOW);
	digitalWrite(relay3, LOW);
}
