//WARNING: If there is any problem with the rails going in the other way
//Change HIGH to LOW in relay1 relay2 relay3 and revert.

//Adding the stop variable
boolean stop = false;

//Adding the lights
const char red = 11;
const char yellow = 10;
const char green = 9;

//Adding the relays
const char relay1 = 6;
const char relay2 = 5;
const char relay3 = 4;

//Adding the sensors
const char breakSensor = 3;
const char outSensor = 2;

//Adding the COM
const char COMin = 8;
const char COMout = 7;

//Define detect
int detect = true;

void setup() {
  //Define the outputs
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);

  //Define the inputs
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(8, INPUT);
}

void loop() {
  //When receive a signal:
  detect = digitalRead(COMin);
  if(detect == HIGH) {
    //Set stop to false
    stop = false;
    Serial.println("The zone will start");

    //Set the lights to green
    digitalWrite(green, HIGH);
    digitalWrite(red, LOW);
    digitalWrite(yellow, LOW);

    //Powering the rails
    digitalWrite(relay1, HIGH);
    digitalWrite(relay2, HIGH);
    digitalWrite(relay3, HIGH);
  }

  //When the train goes out the block:
  detect = digitalRead(outSensor);
  if(detect == LOW) {
    //Send start to the precedent
    digitalWrite(COMout, HIGH);

    //Set lights to red
    digitalWrite(green, LOW);
    digitalWrite(red, HIGH);
    digitalWrite(yellow, LOW);

    //Set stop to true
    stop = true;

    //Stop to power the COMout
    delay(500);
    digitalWrite(COMout, LOW);
  }

  //If the train enter the breaking zone and needs to stop
  detect = digitalRead(breakSensor);
  if(detect == LOW && stop == true) {
    //Power off the tracks.
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
    digitalWrite(relay3, LOW);
  }
}
