# STBS: A simple train blocking system

The simple train blocking system. An easy to install train blocking system using Arduino. Compatible for MFX and DCC++ decoders.

[TOC]



## Why we made this

This project comes from another project called ATBS (Arduino Train Blocking System) the problem with this was that it was barely reproductible. I mean that we made 3 zones (3 blocks) and each time we needed to add one zone or one block we needed to change the code and all the hardware and stuff. 

Also, there was so much wires that it was impossible to troubleshoot.

This project has the same goal as ATBS: prevent accidents using a train blocking system (as in IRL) for Märklin trains as well as DCC++ decoders. But STBS is also very easy and fast to build. 

The principle is that there is one STBS unit by block, and each unit is connected with the next one and the precedent one. 

## How that works

The code is separated in 3 parts. 

| When receive a signal from the next unit | When the train goes out the unit          | If the train needs to stop and is in the stop zone |
| ---------------------------------------- | ----------------------------------------- | -------------------------------------------------- |
| Set lights to green                      | Send a signal to the precedent unit       | Power off the security track                       |
| Power the rails with AC power            | Set the lights of the current unit to red | Power the main tracks of the zone with DC power.   |
| Set the variable stop to false           | Set the variable stop to true             |                                                    |

DC power for a train means 'stop' (but the lights are still activated and it stops smoothly)

AC power for a train means 'start'

In this project we uses IR sensors (there are 2 of them, one for the stop zone, and another one to detect when the train goes out the zone)

This is a very cheap solution compared to the official one from Märklin and easier to install compared to the initial version.

Also to control the whole network a Bluetooth module will be added to reset, power off and test the network.

## Features

* Supports Märklin train without a Märklin solution
* Support DCC++ decoders as well
* Easy and fast to build
* 100% open-source software and hardware

For the future features

* Support for other types of sensors
* Orange light for testing and desactivation of the system
* Control panel to choose one specific block to stop through a computer.
* Add a better security track support
* etc.

## Building

I'll do a PCB board later to be much more easy to make and also make images.

### Build from source (only option for know)

You can find all the infromations about the build instructions in the [building guide](./build-guide.pdf)

## Contributing

### Issues

1. Check issues to answer and contribute to them first
2. Check if your issue already exist
3. If not, create one by describing as much as possible what you want or need. And propose solutions.
4. Check if new comments arrives at your issue
5. Close your issue when you got your answer.

### Pull requests

1. If your enhancement is big, create an issue first
2. Click on 'Fork'
3. Make your changes in the files
4. Go back on the original project and click on 'New Pull request' 
5. Describe your changes and why they should be added
6. Check about critics and maybe solve issues about it
7. You'll know if your project is merged

## How to use

1. Power up the whole thing, no light should work at this point.
2. Add trains on the tracks and control them
3. Everything should works. If it don't please, think about creating an issue.

## Meta

This project is under GNU-GPLv3 license. More informations [here](LICENSE)



