# Contributing guide

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:
The following is a set of guidelines for contributing. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by the [Contributor Covenant](./CODE_OF_CONDUCT.md).

## I don't want to read the whole thing I just have a question!!!

Feel free to post your question in the issues.

## What should I know before getting started?

The hardware schema has been designed with the open-source tool `Fritzing` and the software has been compiled and uploaded using the `ino` tool. 

But you can also simply copy and paste the code into `Arduino IDE` if you prefer using this software. The code is located [here](./software/ino/src/sketch.ino)

If you want more informations about the building process you can go checkout the [building guide](./build-guide.pdf). Or ask questions in the issues.

Also know that this project has **no** commercial purposes and is fully open-source, and will always be.

## How to contribute?

### Reporting bugs

1. Make sure your question / bug cannot be solved using the FAQ and other informations above
2. Check the other issues to know if your issue exist. If it already exist, please, just comment under it and don't create a new one.
3. If your issue is new, then create an issue with a clear and descriptive title.
4. Follow the template for the content:

```markdown
## Environement

* OS:
* Version of the software:
* Arduino board used: 

## Actual behaviour

This what's happening: 

### Logs

This is the error I got and this is the logs from the serial port: 

## Expected behaviour

This is what I would like to do: 

## Reproduction of the issue

To reproduce my issue, follow these steps:

1. 
2. 
3. 

I searched on the web and in the provided resources but I couldn't find my issue. 

Can you help me?

Thanks in advance!
```

5. Please stay active on the issue and close it only when you think the discussion is over.

### Suggestions / Enhancements

1. Check if there is already an issue about it, if so, try to contribute to it by adding your ideas.
2. If the issue doesn't exist, create one with a very clear and descriptive title
3. Follow this template to make your suggestion:

```markdown
## Environement

* Arduino board used: 

## The idea

This is the idea I got...

## Step by step description

## Why it would be good to implement it

## Screenshot

This is a screenshot to show you how it could be implemented in the interface:

![screenshot](URLTOSCREENSHOT)
```

4. Please stay active on the issue and close it when you think the discussion is over.


### Your first code contribution

Unsure where to begin contributing to the project? 

1. Start by looking at `help-wanted` and `begginer` issues. These are issues that often doesn't require much code.
2. Click on Fork and clone the repository to make your changes
3. Make sure your contribution is useful for the project
	* Maintain the code quality
	* Fix problems and bugs
	* Create sustainable solutions for the project
4. When you code please respect the [style](#styleguides)
5. Once you submitted your pull request, make sure to take care about the recommendations of the community.

## Styleguides

### Git commit messages

Please use the following synthax for your git commit messages:

* Just try to be descriptive in your commits messages.

### Arduino Language

This is the style: 

* The code must be commented
* The code must have debuging using `Serial.println()` in the code to be more easier to understand.

### Hardware schematics

For the fritzing files:

* If you are using a specific module in fritzing, please provide it in your repo
* Add notes
* Use the same color types and logic than in the provided schema
* Make sure your schema isn't too messy by organizing the wires.
